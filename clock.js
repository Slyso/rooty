const getTime = () => {
  const date = new Date();

  let hours = date.getHours();
  let minutes = date.getMinutes();

  if (hours < 10) hours = '0' + hours;
  if (minutes < 10) minutes = '0' + minutes;

  return hours + ':' + minutes;
};

const getDate = () => {
  const date = new Date();
  const weekday = date.toLocaleString('default', { weekday: 'long' });
  const month = date.toLocaleString('default', { month: 'long' });

  return weekday + ', ' + date.getDate() + '. ' + month;
};

const updateClockAndDate = () => {
  const clock = document.getElementById('clock');
  clock.textContent = getTime();

  const date = document.getElementById('date');
  date.textContent = getDate();
};
