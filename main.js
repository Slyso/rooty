window.onload = () => {
  updateClockAndDate();
  loadBackgroundImage();
  initSettings();
};

window.addEventListener('keydown', (e) => {
  if(activeDialog) dialogOnKeydown(e);
  else bookmarkSearchOnKeydown(e);
});

setInterval(() => {
  updateClockAndDate();
}, 5000);
