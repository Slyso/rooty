const loadBackgroundImage = () => {
  const amountOfImages = 14;

  const image = Math.floor(Math.random() * amountOfImages) + '.jpg';
  document.body.style.backgroundImage = `url('images/${image}')`;
};
